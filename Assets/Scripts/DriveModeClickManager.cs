﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleARCore;

public class DriveModeClickManager : MonoBehaviour
{
    [SerializeField] DriveModeARController driveModeAR;
    [SerializeField] ColliderManager colliderManager;

    public void ResetButton()
    {
        if (driveModeAR.listObjectFromPreviousScene != null)
        {
            foreach (var gameObjectFromPrevousScene in driveModeAR.listObjectFromPreviousScene)
            {
                if (gameObjectFromPrevousScene.gameObject.name != "ARCore Device")
                {
                    gameObjectFromPrevousScene.gameObject.SetActive(true);
                }
            }
        }
        var listToDestroyAnchor = FindObjectsOfType<Anchor>();
        foreach (var anchor in listToDestroyAnchor)
        {
            Destroy(anchor.gameObject);
        }
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        colliderManager.wallsLengthList.Clear();
        driveModeAR.flagList.Clear();
        driveModeAR.carExist = false;
        driveModeAR.placeToDriveExist = false;
        driveModeAR.howManyAnchors = 0;
        driveModeAR.firstShootAnchor = false;
    }
    public void LoadScene()
    {
        SceneManager.LoadScene(0); 
    }
}
