﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;


public class ColliderManager : MonoBehaviour
{
    [SerializeField] private GameObject barrierPrefab;

    public List<float> wallsLengthList = new List<float>();

    private float wallLengthFromDistance;
    private Vector3 directionToRotateWall;
    private Vector3 wallPosition;
    private Vector3 directionFromTwoFlags;
    private MeshRenderer wallMesh;
    private GameObject wallGameObject;


    public void MakeCollider(List<Transform> flags)
    {
        for (int i = 0; i < 3; i++)
        {
            wallLengthFromDistance = Vector3.Distance(flags[i].position, flags[i + 1].position);

            wallPosition = Vector3.Lerp(flags[i].position, flags[i + 1].position, 0.5f);

            directionFromTwoFlags = flags[i].position - flags[i + 1].position;

            directionToRotateWall = Vector3.Cross(Vector3.up, directionFromTwoFlags).normalized;

            wallGameObject = Instantiate(barrierPrefab, wallPosition, Quaternion.Euler(new Vector3(0f, 0f, 0f)));

            wallGameObject.transform.localRotation = Quaternion.LookRotation(directionToRotateWall);

            wallMesh = wallGameObject.GetComponentInChildren<MeshRenderer>();

            wallsLengthList.Add(wallLengthFromDistance);

            wallMesh.transform.localScale = new Vector3(wallLengthFromDistance, wallMesh.transform.localScale.y, wallMesh.transform.localScale.z);

            wallGameObject.transform.parent = flags[i].transform;
        }


        wallLengthFromDistance = Vector3.Distance(flags[3].position, flags[0].position);
        wallPosition = Vector3.Lerp(flags[3].position, flags[0].position, 0.5f);
        directionFromTwoFlags = flags[3].position - flags[0].position;
        directionToRotateWall = Vector3.Cross(Vector3.up, directionFromTwoFlags).normalized;
        wallGameObject = Instantiate(barrierPrefab, wallPosition, Quaternion.Euler(new Vector3(0f, 0f, 0f)));
        wallGameObject.transform.localRotation = Quaternion.LookRotation(directionToRotateWall);
        wallMesh = wallGameObject.GetComponentInChildren<MeshRenderer>();
        wallsLengthList.Add(wallLengthFromDistance);
        wallMesh.transform.localScale = new Vector3(wallLengthFromDistance, wallMesh.transform.localScale.y, wallMesh.transform.localScale.z);
        wallGameObject.transform.parent = flags[3].transform;
    }



}
