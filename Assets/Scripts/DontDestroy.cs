﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{

    public static DontDestroy instance = null;
    private GameObject dontDestroyObject = null;

    private void Start()
    {
        dontDestroyObject = GameObject.Find("DontDestroyOnLoad");
        if (dontDestroyObject == null)
        {
            DontDestroyOnLoad(gameObject);
        }
    }



}
