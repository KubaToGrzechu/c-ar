﻿using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

#if UNITY_EDITOR
using Input = GoogleARCore.InstantPreviewInput;
#endif

public class DriveModeARController : MonoBehaviour
{

    [SerializeField] private Camera FirstPersonCamera;
    [SerializeField] private GameObject DetectedPlanePrefab;
    [SerializeField] private GameObject flagPrefab;
    [SerializeField] private GameObject carPrefab;
    [SerializeField] private ColliderManager colliderManager;
    [SerializeField] private bl_Joystick joystickPlayer;

    public GameObject planeGeneratior;
    public GameObject discoveryPointCloud;
    public List<Transform> flagList = new List<Transform>();
    public DontDestroy[] listObjectFromPreviousScene;
    public bool placeToDriveExist = false;
    public bool carExist = false;
    public bool firstShootAnchor = false;
    public int howManyAnchors = 0;


    private CarConfigSave carConfig;
    private GameObject flag;
    private GameObject car;
    private Anchor anchor;
    private const float k_ModelRotation = 180.0f;
    private bool m_IsQuitting = false;


    private void Awake()
    {
        carConfig = FindObjectOfType<CarConfigSave>();
        joystickPlayer.gameObject.SetActive(false);
    }


    public void Update()
    {
        _UpdateApplicationLifecycle();

        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began || carExist == true)
        {
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            Debug.Log("touched UI");
            return;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            if ((hit.Trackable is DetectedPlane) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else if (howManyAnchors < 4)
            {
                if (hit.Trackable is DetectedPlane)
                {
                    DetectedPlane detectedPlane = hit.Trackable as DetectedPlane;
                    if (detectedPlane.PlaneType == DetectedPlaneType.Vertical)
                    {
                        Debug.Log("cant place vertical");
                        return;
                    }
                }
                else
                {
                    return;
                }


                flag = Instantiate(flagPrefab, hit.Pose.position, hit.Pose.rotation);

                flag.transform.Rotate(0, 0, 0, Space.Self);


                if (firstShootAnchor == false)
                {
                    anchor = hit.Trackable.CreateAnchor(hit.Pose);
                    firstShootAnchor = true;
                }


                flagList.Add(flag.transform);

                flag.transform.parent = anchor.transform;

                howManyAnchors++;

            }
            else if (howManyAnchors == 4 && placeToDriveExist == false)
            {
                colliderManager.MakeCollider(flagList);
                placeToDriveExist = true;
                return;
            }
            else if (placeToDriveExist == true && carExist == false)
            {
                Vector3 carMiddlePosition = Vector3.Lerp(flagList[0].position, flagList[2].position, 0.5f);

                car = Instantiate(carConfig.carPreferences, carMiddlePosition, Quaternion.Euler(0f, 0f, 0f).normalized);

                car.SetActive(true);

                car.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);

                Rigidbody rigidbody = car.AddComponent<Rigidbody>();


                rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                rigidbody.mass = 2.6f;
                rigidbody.drag = 0.9f;
                rigidbody.angularDrag = 1f;


                carExist = true;

                car.AddComponent<DriveController>();

                car.transform.parent = anchor.transform;

                joystickPlayer.gameObject.SetActive(true);

                listObjectFromPreviousScene = FindObjectsOfType<DontDestroy>();

                foreach (var gameObjectFromPrevousScene in listObjectFromPreviousScene)
                {
                    if (gameObjectFromPrevousScene.gameObject.name != "ARCore Device")
                    {
                        gameObjectFromPrevousScene.gameObject.SetActive(false);
                    }
                }
            }
        }
    }


    private void _UpdateApplicationLifecycle()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
        if (m_IsQuitting)
        {
            return;
        }
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            _ShowAndroidToastMessage(
                "ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }

    private void _DoQuit()
    {
        Application.Quit();
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity =
            unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject =
                    toastClass.CallStatic<AndroidJavaObject>(
                        "makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
    }
}

