﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GoogleARCore;
using System.Collections;




public class ClickManager : MonoBehaviour
{
    [SerializeField] private Text _rotationText;
    [SerializeField] private Text _sizeText;
    [SerializeField] private float timeToOpenWindow = 5f;

    public MainARController _mainARController;

    private List<GameObject> spoilersList = new List<GameObject>();
    private List<GameObject> wheelsList = new List<GameObject>();
    private List<GameObject> frontLight = new List<GameObject>();
    private Transform[] carGameObjects;
    private GameObject hoodReferences;
    private Vector3 firstHoodPosition;
    private Vector3 localScale;
    private int _sizeValue = 5;
    private int _rotationValue = 90;
    private int NumberActiveWheels = 1; //zestaw kół
    private int NumberActiveSpoiler = 0; //zestaw
    private bool rightWindowsAction = false;
    private bool leftWindowsAction = false;
    private bool rightWindowOpen = false;
    private bool leftwindowOpen = false;
    private bool lightsEventOn = false;
    private bool hoodAction = false;
    private bool hoodOpen = false;


    private void Start()
    {
        _rotationText.text = _rotationValue.ToString();
        localScale = Vector3.one * _sizeValue / 100;
        _sizeText.text = _sizeValue.ToString();
    }

    public void AddListOfCarParts()
    {
        carGameObjects = _mainARController.carObject.transform.GetComponentsInChildren<Transform>(true);

        SpoilersListCreator();
        WheelsListCreator();
        MaskTransformCreator();
        FrontLightListCreator();
    }

    private void FrontLightListCreator()
    {
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.tag == "FrontLight")
            {
                frontLight.Add(carGameObject.gameObject);
            }
        }
    }

    private void MaskTransformCreator()
    {
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.gameObject.name == "Hood")
            {
                firstHoodPosition = carGameObject.localRotation.eulerAngles;
            }
        }
    }

    private void SpoilersListCreator()
    {
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.tag == "spoiler")
            {
                spoilersList.Add(carGameObject.gameObject);
            }
        }
    }

    public void LoadNewScene()
    {
        CarConfigSave carConfig = FindObjectOfType<CarConfigSave>();

       
        if (_mainARController.carObject == null)
        {
            Debug.Log("enter0");
            GameObject clone;
            clone = Instantiate(_mainARController.carPrefab);
            clone.transform.parent = carConfig.gameObject.transform;
            carConfig.carPreferences = clone;
            clone.SetActive(false);
        }
        else
        {
            _mainARController.carObject.transform.parent = carConfig.gameObject.transform;
            carConfig.carPreferences = _mainARController.carObject;
            carConfig.transform.localPosition = new Vector3(0, 0, 0);
            carConfig.transform.localRotation = Quaternion.Euler(Vector3.zero);
            carConfig.transform.localScale = new Vector3(1, 1, 1);
            _mainARController.carObject.SetActive(false);
            if (hoodReferences != null)
            {
                hoodReferences.transform.localRotation = Quaternion.Euler(firstHoodPosition);
            }
            RemoveCar();
        }


        SceneManager.LoadScene(1);
    }

    private void WheelsListCreator()
    {
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.tag == "wheels")
            {
                wheelsList.Add(carGameObject.gameObject);
            }
        }
    }

    public void RemoveCar()
    {
        Destroy(GameObject.Find("Anchor"));
        _mainARController.carExist = false;

        wheelsList.Clear();
        spoilersList.Clear();

        NumberActiveSpoiler = 0;
        NumberActiveWheels = 1;

        _mainARController.planeGeneratior.SetActive(true);
    }

    //change spoiler
    public void AddSpoiler()
    {

        if (NumberActiveSpoiler == spoilersList.Count)
        {
            for (int i = 0; i < spoilersList.Count; i++)
            {
                spoilersList[i].SetActive(false);
            }
            NumberActiveSpoiler = 0;
            return;
        }

        for (int i = 0; i < spoilersList.Count; i++)
        {
            if (i == NumberActiveSpoiler)
            {
                spoilersList[i].SetActive(true);
            }
            else
            {
                spoilersList[i].SetActive(false);
            }
        }
        NumberActiveSpoiler++;
    }

    public void ChangeWheels()
    {
        if (NumberActiveWheels == wheelsList.Count)
        {
            NumberActiveWheels = 0;
            SetWheelsAcitve();
        }
        else
        {
            SetWheelsAcitve();
        }
    }

    public void LightEvens()
    {
        if (lightsEventOn == false)
        {
            lightsEventOn = true;
            StartCoroutine(LightEvensOnTime());
        }
        else
        {
            StartCoroutine(LightEvensOnTime());
            lightsEventOn = false;
        }
    }
    private IEnumerator LightEvensOnTime()
    {
        bool isLightsOn = false;
        while (true)
        {
            for (int i = 0; i < frontLight.Count; i++)
            {
                frontLight[i].SetActive(isLightsOn);
            }
            yield return new WaitForSeconds(1);
            isLightsOn = !isLightsOn;
        }
    }

    private void SetWheelsAcitve()
    {
        for (int i = 0; i < wheelsList.Count; i++)
        {
            if (i == NumberActiveWheels)
            {
                wheelsList[i].SetActive(true);
            }
            else
            {
                wheelsList[i].SetActive(false);
            }
        }
        NumberActiveWheels++;
    }


    public void OpenHood()
    {
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.gameObject.name == "Hood")
            {
                if (hoodAction == false)
                {
                    hoodAction = true;

                    if (hoodOpen == false)
                    {
                        StartCoroutine(OpenMaskOverTime(carGameObject));
                        hoodOpen = true;
                    }
                    else
                    {
                        StartCoroutine(CloseMaskOverTime(carGameObject));
                        hoodOpen = false;
                    }
                }
            }
        }
    }
    private IEnumerator OpenMaskOverTime(Transform maskPivotPoint)
    {

        hoodReferences = maskPivotPoint.gameObject;

        for (float i = 0; i < timeToOpenWindow; i += Time.deltaTime)
        {
            Debug.Log(i);
            Vector3 lerpRotation = Vector3.Lerp(firstHoodPosition, new Vector3(-90f, 0f, 0f), i / timeToOpenWindow);
            maskPivotPoint.localRotation = Quaternion.Euler(lerpRotation);
            yield return null;
        }
        hoodAction = false;
    }
    private IEnumerator CloseMaskOverTime(Transform maskPivotPoint)
    {
        Vector3 firstPosition = maskPivotPoint.localRotation.eulerAngles;

        for (float i = 0; i < timeToOpenWindow; i += Time.deltaTime)
        {
            Debug.Log(i);
            Vector3 lerpRotation = Vector3.Lerp(new Vector3(-90f, 0f, 0f), firstHoodPosition, i / timeToOpenWindow);
            maskPivotPoint.localRotation = Quaternion.Euler(lerpRotation);
            yield return null;
        }
        hoodAction = false;
    }

    public void LeftWindowAction()
    {
        Debug.Log("enter left widnow");
        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.gameObject.name == "LeftWindow")
            {
                if (leftWindowsAction == false)
                {
                    leftWindowsAction = true;

                    if (leftwindowOpen == false)
                    {
                        StartCoroutine(OpenWindowInTime(carGameObject, new Vector3(-0.095f, -0.35f, 0.085f)));
                        leftwindowOpen = true;
                    }
                    else
                    {
                        StartCoroutine(OpenWindowInTime(carGameObject, Vector3.zero));
                        leftwindowOpen = false;
                    }
                }
            }
        }
    }
    public void RightWindowAction()
    {

        foreach (var carGameObject in carGameObjects)
        {
            if (carGameObject.gameObject.name == "RightWindow")
            {
                if (rightWindowsAction == false)
                {
                    rightWindowsAction = true;

                    if (rightWindowOpen == false)
                    {
                        StartCoroutine(OpenWindowInTime(carGameObject, new Vector3(0.095f, -0.375f, 0.085f)));
                        rightWindowOpen = true;
                    }
                    else
                    {
                        StartCoroutine(OpenWindowInTime(carGameObject, Vector3.zero));
                        rightWindowOpen = false;
                    }
                }
            }
        }
    }
    IEnumerator OpenWindowInTime(Transform window, Vector3 target)
    {
        float startTime = Time.time;
        Vector3 windowStartingPoint = window.localPosition;
        for (float i = 0; i < timeToOpenWindow; i += Time.deltaTime)
        {
            Debug.Log("petla");
            window.localPosition = Vector3.Lerp(windowStartingPoint, target, i / timeToOpenWindow);
            yield return new WaitForEndOfFrame();
        }
        window.localPosition = target;
        Debug.Log("Endcorutine");
        if (window.name == "RightWindow")
        {
            rightWindowsAction = false;
        }
        else
        {
            leftWindowsAction = false;
        }
    }

    public void ChangeCarColor(string stringColor)
    {
        if (ColorUtility.TryParseHtmlString(stringColor, out Color color) == false)//HTML STRING?
        {
            return;
        }
        MeshRenderer[] meshRenderers;
        meshRenderers = _mainARController.carObject.GetComponentsInChildren<MeshRenderer>(true);
        foreach (var mesh in meshRenderers)
        {
            if (mesh.tag == "CarColor")
            {

                mesh.material.color = color;
            }
        }
    }

    public void ChangeSizeValue(int changingValue)
    {
        if (_sizeValue + changingValue < 1 || _sizeValue + changingValue > 10) { }
        else
        {
            localScale = Vector3.one * _sizeValue / 100;
            _sizeValue += changingValue;

            _sizeText.text = _sizeValue.ToString();
        }
    }

    public void RescaleCar(bool positiveOrNegative)
    {
        if (positiveOrNegative == true)
        {
            if (_mainARController.carObject.transform.localScale.x + localScale.x < 1f)
            {
                _mainARController.carObject.transform.localScale += localScale;
            }
        }
        else
        {
            if (_mainARController.carObject.transform.localScale.x - localScale.x > 0f)
            {
                _mainARController.carObject.transform.localScale -= localScale;
            }
        }
    }

    public void ChangeAngleRotation(int changeValueRotation)
    {
        if (_rotationValue + changeValueRotation < 5 /*== false*/ || _rotationValue + changeValueRotation > 180 /*== false*/)
        {
            //do nothing
        }
        else
        {
            _rotationValue += changeValueRotation;
            _rotationText.text = _rotationValue.ToString();
        }
    }

    public void RotateCar(int direction)
    {
        Vector3 rotation = new Vector3
            (
            0.0f,
            (direction * _rotationValue),
            0.0f
            );
        Vector3 currentRotation = _mainARController.carObject.transform.localRotation.eulerAngles;
        Vector3 newRotation = currentRotation + rotation;
        _mainARController.carObject.transform.localRotation = Quaternion.Euler(newRotation);
    }
}
