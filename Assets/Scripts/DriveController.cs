﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DriveController : MonoBehaviour
{
    [SerializeField] private float carSpeed = 4f;
    [SerializeField] private float rotationFromCamera = -90f;

    private ColliderManager colliderManager;
    private Vector3 angleRotarionDisplay;
    private Vector3 directionOfCamera;
    private Transform cameraTransform;
    private Rigidbody carRigidbody;
    private bl_Joystick joystick;
    private Text text;
    private float rotation;
    private float translation;
    private float verticalValue;
    private float horizontalValue;
    private float smallestWallLength;


    private void Start()
    {
        text = GameObject.Find("textinfo").GetComponent<Text>();
        cameraTransform = GameObject.Find("First Person Camera").transform;
        joystick = GameObject.Find("Joystick").GetComponent<bl_Joystick>();

        smallestWallLength = Mathf.Min(GameObject.Find("ColliderManager").GetComponent<ColliderManager>().wallsLengthList.ToArray()); // AJ EM WERY SMOL
        carRigidbody = GetComponent<Rigidbody>();
        directionOfCamera = new Vector3(carRigidbody.transform.position.x - cameraTransform.position.x, 0f, carRigidbody.transform.position.z - cameraTransform.position.z);
        carRigidbody.gameObject.transform.rotation = Quaternion.LookRotation(directionOfCamera).normalized;
    }

    void Update()
    {

        directionOfCamera = new Vector3(carRigidbody.transform.position.x - cameraTransform.position.x, 0f, carRigidbody.transform.position.z - cameraTransform.position.z);

        text.text = carRigidbody.velocity.ToString();

        verticalValue = joystick.Vertical;
        horizontalValue = joystick.Horizontal;

        Debug.DrawLine(new Vector3(carRigidbody.transform.position.x, 0f, carRigidbody.transform.position.z), new Vector3(cameraTransform.position.x, 0f, cameraTransform.position.z));

        if (verticalValue != 0 || horizontalValue != 0)
        {
            translation = verticalValue * Time.deltaTime;
            rotation = horizontalValue * Time.deltaTime;

            Debug.Log(Quaternion.LookRotation(directionOfCamera));
            angleRotarionDisplay = new Vector3(0f, rotationFromCamera + Mathf.Atan2(verticalValue, -horizontalValue) * Mathf.Rad2Deg, 0f);
            transform.rotation = Quaternion.Euler(angleRotarionDisplay).normalized * Quaternion.LookRotation(directionOfCamera).normalized;
            carRigidbody.AddForce(transform.forward * carSpeed * smallestWallLength);
        }
        else
        {
            carRigidbody.velocity = Vector3.zero;
        }
    }
}